<?php

// bootstrap.php
// Load Composer packages
require_once __DIR__.'/../vendor/autoload.php';

// Configure and setup the application
require_once __DIR__.'/config.php';

// Load the required functions
require_once __DIR__.'/api/apiloader.php';

return $app;

?>
