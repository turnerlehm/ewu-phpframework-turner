# EWU PhP Framework Adding/Viewing APIs
In this ReadMe, you will find the proper way to add/view APIs in this framework. By keeping to this structure, it allows for extendability and readability for the framework.

## Where to begin
   Starting from the root directory of the framework, the api functions can be found in /src/api/
   Within this directory there will be other directories followed by apiloader.php.
   
   Each of these other directories contains a php file which contains related api function routes that are apart of the framework. The apiloader.php file contains a ```include __DIR__``` for each of php files within the aditional diretories.
   
  ## Demonstration on finding API functions. 
  In the /src/api/ directory there is a directory named  'test_directory' and the apiloader.php file.
  
  ##### Within the directory /src/api/test_directory/ the file test.php exists and its contents are:
  ```php
  <?php
        $app->match('/this_is_a_test', function(){
	        return 'This is a test';
        });
    ?>
  ```
  * This /src/api/test_directory/test.php file contains all API calls related to testing.
  * The framework is able to find all the test API calls by linking the test.php file to the apiloader.php file.
  
  
 ##### Below are the contents of the apiloader.php file:
 ```php
    <?php
        require_once __DIR__.'/test/test.php'
    ?>
 ```
* The apiloader.php file currently only has 1 file it is including, as more directories for api calls are created, an additional include for each new php file is added to the apiloader.php file. 


 ## Example of adding an API to an existing set of APIs
  Lets say I want to add another testing API function, the perfect place to add this new API would be under the /src/api/test_direcory/test.php as this file contains APIs used for testing.
  
  I want to add a test to make sure my Database is hooked up correctly. (Demonstration on hooking up a database is found in the database hookup ReadMe)
  
  I would add my API code below to the test.php file:
  ```php
$app->match('/testing_db', function(){
    $result =  $app['dbs']['sqlite']->query("SELECT * FROM student");
    if($result){
      return 'Database is working.';
    }
    else{
      return 'Database is not working.';
    }
});
  ```
  * This code successfully added an API call to the framework.
  
## Example of adding an API from a new set of API fucntions
Lets say I want to add a new group of API functions that are not related to any existing groups within the framework. In this example I will create a fictional API set.

1. Create directory in /src/api/ with a related name for the set of API calls.
2. Create the php file in which the API functions will exist in.
3. Add the php file to the apiloader.php file in /src/api/

#### Demonstration
I want to add a set of API functions that deal with database manipulation. First check if such a set of related API calls exist. After verifying there is no related API set, I will create the /src/api/database_functions directory.

Within this directory I will create the database.php file which will contain all database related API calls within.
```php
$app->match('/db', function () use($app){
    $result =  $app['dbs']['sqlite']->query("SELECT * FROM student");
    foreach($result as $row)
    {
        print 'id = '.$row['id']. ' name = "'.$row['fname'].' '.$row['lname'].'"<br>';
    }
    return '';
})->before($ssoProtect);
```

Now that I have my database API function added to my new php file, I need to add the file to the apiloader.php file found in /src/api/
To do this simply add the following code to the apiloader.php file:
```php
    require_once __DIR__.'/database_functions/database.php'
```

My API function is now added to the framework. To add additional database related functions, simply add additional functions to the database.php in this example. 