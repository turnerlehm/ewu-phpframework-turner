<?php

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;




/*
 * This before middleware makes sure the user is SSO Authenticated.
 * @param Symfony\Component\HttpFoundation\Request $request The HTTP request
 * @param Silex\Application $app The application servicing HTTP requests
 */
$ssoProtect = function (Request $request, Application $app) {

    $sso = new EWU\SingleSignOn\SSOClient('SAML_VERSION_1_1');
    try {
        /* Add the netId to the request parameters */
        $results = $sso->authenticate();
        $username = $results['username'];
        $request->headers->set('netId', $username);

        /* Add additional EWU attributes to the request params */
        foreach ($results['attributes'] as $key => $value) {
            $request->headers->set($key, $value);
        }
    } catch (CAS_AuthenticationException $e) {
        return new Symfony\Component\HttpFoundation\Response('An authentication error occurred', 401);
    }
};

// Protect a page with EWU SSO and Employee Access
$employees_only = function (Request $request, Application $app) {

    $sso = new EWU\SingleSignOn\SSOClient('SAML_VERSION_1_1');
    try {
        $results = $sso->authenticate();
        $username = $results['username'];
        $request->headers->set('netid', $username);
        $isEmployee = false;
        foreach ($results['attributes'] as $key => $value) {
            $request->headers->set($key, $value);
            if ($key == 'UserType' && $value == 'Employee') {
                $isEmployee = true;
            }
        }

        if (!$isEmployee) {
            return new Symfony\Component\HttpFoundation\Response('This site is accessible only by EWU employees', 401);
        }
    } catch (CAS_AuthenticationException $e) {
        return new Symfony\Component\HttpFoundation\Response('An authentication error occurred', 401);
    }
};

